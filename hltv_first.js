#!/usr/bin/env node
/*
 * Copyright 2016 Dmitry Nikolaev
 * Licensed with GNU/GPLv3, see LICENSE file
 * This file is part of HLTV First

 * HLTV First is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * HLTV first is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with HLTV First. If not, see <http://www.gnu.org/licenses/>.
 */
var lastUrl;
var isFirst = 1;
const request = require('req-fast');
const TelegramBot = require('node-telegram-bot-api');
const fs = require('fs');
const parseString = require('xml2js').parseString;
const token = "275418166:AAG_MnMm2dxZgFMpymxJUeYqw3Fl2ujEvXc";
const bot = new TelegramBot(token, { polling: true });
const message = "lel";

function requester(hltvUrl, method, data, callback){
	hdr = {
		'Accept-Encoding': 'gzip, deflate',
		'Accept-Language': 'en-US,en;q=0.8,ru;q=0.6',
		'Cookie': 'PHPSESSID=YOUR_cookie; autologin=YOUR_cookie'
	};
	console.log('[debug] req data: ', data);
	request({
		url: hltvUrl,
		method: method,
		headers: hdr,
		dataType: 'form',
		data: data
	}, (error, response) => {
		console.log("[err]",error)
		if (error || response.status == 500) {
			console.log(error);
			return requester(hltvUrl, method, data, callback);
		}
		console.log('[status] requester ok');
		return callback(response);
	});
}

function response(data){
	console.log("[status] comment sent! :D");
	bot.sendMessage(154161879, "[HLTV][status] comment should be sent:D");
	//console.log(data);
}

function pThreadId(data){
	console.log("[status] Getting thread id..");
	var id = parseInt(data.body.split("threadid")[1].split("\"")[2]);
	console.log("[status] thread id is: " + id);
	requester('http://www.hltv.org/?pageid=35&threadid=' + id, 'POST', 
	 {replymsg:message, threadid:id, replytoid:0, action:'reply', 'replysubmit.x':22, 'replysubmit.y':5, replysubmit:'Submit+reply'}, response);
	//replymsg=ez4ence&threadid=id&replytoid=0&action=reply&replysubmit.x=28&replysubmit.y=12&replysubmit=Submit+reply
	//requester('http://www.hltv.org/?pageid=35&threadid=' + id, 'POST', "replymsg=ez+4+ENCE&threadid=" + id +  "&replytoid=0&action=reply&replysubmit.x=22&replysubmit.y=5&replysubmit=Submit+reply", response);
	
}

function parseNews(xml){
	console.log("[status] parsing news..");
	parseString(xml.body, function (err, result) {
		var curUrl = result.rss.channel[0].item[0].link[0];
		console.log("[status] Current post: " + curUrl);
		//isFirst = false; //debug
		if(isFirst){
			isFirst = 0;
			lastUrl = curUrl;
			return;
		}
		if(lastUrl != curUrl){
			bot.sendMessage(154161879, "[HLTV][status] New post found!");
			lastUrl = curUrl;
			console.log(requester(curUrl, 'GET', {}, pThreadId)); // need to get thread id
		}
	});
}
function getHltvNews(){
	requester('http://www.hltv.org/news.rss.php', 'GET', {}, parseNews);
//replymsg=ez+4+ENCE&threadid=1323489&replytoid=0&action=reply&replysubmit.x=22&replysubmit.y=5&replysubmit=Submit+reply
}

console.log("[status] starting..");
bot.sendMessage(154161879, "[HLTV][status] starting..");
//getHltvNews();
setInterval(getHltvNews, 1000);
bot.on('message', (msg) => bot.sendMessage(154161879, JSON.stringify(msg)))
